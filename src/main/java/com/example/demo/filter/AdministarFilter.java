package com.example.demo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.demo.entity.User;

public class AdministarFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		// セッション
		HttpSession session = req.getSession();

		// セッション情報からログインユーザー情報を取り出す
		User loginUser = (User) session.getAttribute("loginUser");

		// 管理者でない場合は、TOPページへ遷移する
		if (loginUser.getDepartmentId() != 1) {
			session.setAttribute("adminError", "管理者権限がありません");

			res.sendRedirect("/try");
			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig config){
	}

	@Override
	public void destroy() {
	}
}
