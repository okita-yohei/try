package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Getter
@Setter
public class User {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
//	@NotEmpty(message = "アカウントは必須です")
//	@Size(min = 8, max = 20, message = "パスワードは8文字以上20文字以下で入力してください")
	private String account;
//	@NotEmpty(message = "パスワードは必須です")
	private String password;
//	@NotEmpty(message = "名前は必須です")
    private String name;
    private Integer branchId;
    private Integer departmentId;
    private Integer isStopped;
    @PrePersist
    public void onPrePersist() {
        setCreatedDate(new Date());
        setUpdatedDate(new Date());
    }

    @PreUpdate
    public void onPreUpdate() {
        setUpdatedDate(new Date());
    }

    private Date createdDate;
    private Date updatedDate;
}

