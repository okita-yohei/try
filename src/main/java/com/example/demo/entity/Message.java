package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "messages")
@JsonIgnoreProperties({ "hibernateLazyInitializer" })
@Getter
@Setter
public class Message {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String title;
	@Column
	private String text;
	@Column
	private String category;
	@Column
	private Integer userId;

	@PrePersist
	public void onPrePersist() {
		setCreatedDate(new Date());
		setUpdatedDate(new Date());
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedDate(new Date());
	}

	@Column
	private Date createdDate;
	@Column
	private Date updatedDate;
}
