package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.MessagesService;

@RequestMapping("/message")
@Controller
public class MessageController {
	@Autowired
	MessagesService messageService;
	@Autowired
	HttpSession session;

	//投稿ページ表示
	@GetMapping("/newMessage")
	public ModelAndView newMessage() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("message");
		Message message = new Message();
		mav.addObject("messageForm", message);
		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);
		return mav;
	}

	//新規投稿
	@PostMapping("/addMessage")
	public ModelAndView addMessage(@Validated @ModelAttribute("messageForm") Message message, BindingResult result) {
		String text = message.getText();
		String title = message.getTitle();
		String category = message.getCategory();
		List<String> errorMessages = new ArrayList<String>();

		if(!isValidMessage(title, category, text, errorMessages)) {
			ModelAndView mav =  new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("formModel", message);
			mav.setViewName("message");
			return mav;
		}

		User loginUser = (User) session.getAttribute("loginUser");
		message.setUserId(loginUser.getId());
		messageService.saveMessage(message);
		return new ModelAndView("redirect:/");
	}

	//新規投稿バリデーション
	private boolean isValidMessage(String title, String category, String text ,List<String> errorMessages) {

		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください");
		} else if(30 < title.length()) {
			errorMessages.add("件名は30文字以下で入力してください");
		}

		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリを入力してください");
		} else if (10 < category.length()) {
			errorMessages.add("カテゴリは10文字以下で入力してください");
		}

		if (StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください");
		}else if(text.length() > 1000) {
			errorMessages.add("本文は1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}