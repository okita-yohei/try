package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.repository.BranchRepository;
import com.example.demo.repository.DepartmentRepository;
import com.example.demo.service.UsersService;

@Controller
public class UserController {
	@Autowired
	HttpSession session;
	@Autowired
	UsersService usersService;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	BranchRepository branchRepository;

	//ユーザー登録画面
	@GetMapping("signup")
	public ModelAndView signup(RedirectAttributes redirectAttributes, Model model) {
		ModelAndView mav = new ModelAndView();

		//エラーメッセージ格納用List)
		List<String> signupErrors = (List<String>) model.getAttribute("signupErrors");
		mav.addObject("signupErrors" , signupErrors);

//		バリデーションエラー時にフォームに内容を保持する
		if(model.getAttribute("user") != null) {
			User user = (User) model.getAttribute("user");
		}

		//departmentとbranchを全権取得
		List<Department> departments = departmentRepository.findAll();
		mav.addObject("departments", departments);
		List<Branch> branches = branchRepository.findAll();
		mav.addObject("branches", branches);

//		バリデーションエラー時にフォームに内容を保持する
		if(model.getAttribute("user") != null) {
			User user = (User) model.getAttribute("user");
			mav.addObject("formModel", user);
		} else {
			User user = new User();
			mav.addObject("formModel", user);
		}
		mav.setViewName("/signup");
		return mav;
	}

	//ユーザー新規登録処理
	@PostMapping("/signupuser")
	public ModelAndView addUser(@ModelAttribute("formModel") User user,
			BindingResult result,
			@RequestParam(name = "verficPass")String verficPass,
			@RequestParam(name = "departmentId") Integer departmentId,
			@RequestParam(name = "branchId") Integer branchId,
			RedirectAttributes redirectAttributes) {

		user.setDepartmentId(departmentId);
		user.setBranchId(branchId);
		user.setIsStopped(0);
		//パスワードをエンコード(未実装)
		//			String encPassword = new BCryptPasswordEncoder().encode(user.getPassword());
		//			user.setPassword(encPassword);

		//バリデーション
		List<String> signupErrors = new ArrayList<>();
		if (!signupValid(signupErrors, user, verficPass)) {
			redirectAttributes.addFlashAttribute("user", user);
			redirectAttributes.addFlashAttribute("signupErrors", signupErrors);
			return new ModelAndView("redirect:/signup");
		}

		usersService.saveUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/user");
	}

	//ログイン画面
	@GetMapping("/login")
	public ModelAndView login(RedirectAttributes redirectAttributes, Model model) {
		ModelAndView mav = new ModelAndView();
		//エラーメッセージ格納用リスト
		List<String> loginErrors = (List<String>) model.getAttribute("loginErrors");
		mav.addObject("loginErrors", loginErrors);

		if(model.getAttribute("user") != null) {
			User user = (User) model.getAttribute("user");
			mav.addObject("formModel", user);
		} else {
			User user = new User();
			mav.addObject("formModel", user);
		}

		mav.setViewName("/login");

//		ユーザー情報を保持
//		if(loginErrors != null) {
//			User user = (User) model.getAttribute("user");
//			mav.addObject("formModel", user);
//		}
		String filterError = (String) session.getAttribute("filterError");
		mav.addObject("filterError", filterError);
//		セッションのフィルターメッセージを削除
		session.removeAttribute("filterError");
		return mav;
	}

	//ログイン機能
	@PostMapping("/loginuser")
	public ModelAndView login(
			@RequestParam(name = "account") String account,
			@RequestParam(name = "password") String password,
			RedirectAttributes redirectAttributes) throws NullPointerException{

//		バリデーション用User
		User user = new User();
		user.setAccount(account);
		user.setPassword(password);


//		ユーザー情報を取得
		//バリデーション
		List<String> loginErrors = new ArrayList<>();

//		ユーザー情報を取得
		User loginUser = usersService.loginUser(account, password);

		if (!loginValid(loginErrors, user, loginUser)) {
			redirectAttributes.addFlashAttribute("user", user);
			redirectAttributes.addFlashAttribute("loginErrors", loginErrors);
			return new ModelAndView("redirect:/login");
		}

		//		セッションにログイン情報を保持
		session.setAttribute("loginUser", loginUser);
		return new ModelAndView("redirect:/try");
	}

	//ログアウト画面
	@GetMapping("logout")
	public ModelAndView logout() {

		//セッションを切る処理を追加
		session.invalidate();
		return new ModelAndView("redirect:/login");
	}

	//ユーザー一覧画面
	@GetMapping("/user")
	public ModelAndView users() {
		ModelAndView mav = new ModelAndView();
		List<User> userData = usersService.findAllUser();
		mav.addObject("users", userData);
		List<Department> departments = departmentRepository.findAll();
		mav.addObject("departments", departments);
		List<Branch> branches = branchRepository.findAll();
		mav.addObject("branches", branches);
		mav.setViewName("/user");
		return mav;
	}

	//ユーザアカウント操作
	@GetMapping("/user/isStopped/{id}")
	public ModelAndView isStoppedUser(@PathVariable Integer id) {
		User userData = usersService.editUser(id);

		if (userData.getIsStopped() == 0) {
			userData.setIsStopped(1);
		} else {
			userData.setIsStopped(0);
		}
		usersService.saveUser(userData);
		return new ModelAndView("redirect:/user");
	}

	//URLエラー用
	@GetMapping("/edit/")
	public ModelAndView edit(RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView();
		redirectAttributes.addFlashAttribute("urlError", "不正なパラメーターが入力されました");
		return new ModelAndView("redirect:/user");
	}

	//ユーザー編集画面表示
	@GetMapping("/edit/{id}")
	public ModelAndView editUser(Model model, @PathVariable String id, RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView();
		//バリデーションエラーメッセージ格納
		List<String> editErrors = (List<String>) model.getAttribute("editErrors");
		mav.addObject("editErrors" , editErrors);

		//URLが不正な場合のバリデーション
		if (!id.matches("^[0-9]+$")) {
			redirectAttributes.addFlashAttribute("urlError", "不正なパラメーターが入力されました");
			return new ModelAndView("redirect:/user");
		} else {
			Optional<User> user = usersService.findById(Integer.parseInt(id));
			if(!user.isPresent()) {
				redirectAttributes.addFlashAttribute("urlError", "不正なパラメーターが入力されました");
				return new ModelAndView("redirect:/user");
			}
		}

		User userData = usersService.editUser(Integer.parseInt(id));



		//idが不正な場合のバリデーション
		if (!id.toString().matches("^[0-9]+$")) {
 			String errorMessage = "不正なパラメータが入力されました";
			mav.addObject("errorMessage", errorMessage);;
			return new ModelAndView("redirect:/user");
		}


//		バリデーションエラー時にフォームに内容を保持する
		if(model.getAttribute("user") != null) {
			userData = (User) model.getAttribute("user");
			mav.addObject("users", userData);
		}

		mav.addObject("users", userData);
		List<Department> departments = departmentRepository.findAll();
		mav.addObject("departments", departments);
		List<Branch> branches = branchRepository.findAll();
		mav.addObject("branches", branches);
		mav.setViewName("/edit");
		return mav;
	}

	//ユーザー編集処理
	@PostMapping("/edit/{id}")
	public ModelAndView editUser(@PathVariable Integer id,
			@ModelAttribute("users") User user,
			@RequestParam (name = "verficPass") String verficPass,
			RedirectAttributes redirectAttributes) {
		//パスワードが空の場合既存のパスワードを保存
		if(StringUtils.isEmpty(user.getPassword()) && StringUtils.isEmpty(verficPass)) {
			User searchuser = usersService.findOne(user.getId());
			user.setPassword(searchuser.getPassword());
			verficPass = searchuser.getPassword();
		}
		User userData = usersService.editUser(id);
		List<String> editErrors = new ArrayList<>();
		if(!editValid(editErrors, user, userData, verficPass)) {
			redirectAttributes.addFlashAttribute("user", user);
			redirectAttributes.addFlashAttribute("editErrors", editErrors);
			return new ModelAndView("redirect:/edit/{id}");
		}
		user.setCreatedDate(userData.getCreatedDate());
		usersService.saveUser(user);
		return new ModelAndView("redirect:/user");
	}

	// ユーザー登録バリデーション
	private boolean signupValid(List<String> signupErrors, User user, String verficPass) {


		// 名前
		if (StringUtils.isEmpty(user.getName())) {
			signupErrors.add("ユーザー名を入力してください");
		} else {
			// 名前不正
			if (user.getName().length() > 10) {
				signupErrors.add("ユーザー名は10文字以下にしてください");
			}
		}

		// アカウント
		if (StringUtils.isEmpty(user.getAccount())) {
			signupErrors.add("アカウントを入力してください");
		} else {
			// アカウント重複
			List<User> users = usersService.findAllUser();
			for (int i = 0; i < users.size(); i++) {
				if (user.getAccount().equals(users.get(i).getAccount())) {
					signupErrors.add("アカウントが重複しています");
				}
			}
			//アカウント文字数
			if (!user.getAccount().matches("^[A-Za-z0-9]$") && user.getAccount().length() < 6) {
				signupErrors.add("アカウントは6文字以上で入力してください");
			} else if(!user.getAccount().matches("^[A-Za-z0-9]$") && user.getAccount().length() > 20) {
				signupErrors.add("アカウントは20文字以下で入力してください");
			}
		}

		// パスワード
		if (StringUtils.isEmpty(user.getPassword())) {
			signupErrors.add("パスワードを入力してください");
		} else {
			//パスワードと確認用パスワード比較
			if (!(user.getPassword().equals(verficPass))) {
				signupErrors.add("入力したパスワードと確認用パスワードが一致しません");
			}

			// パスワード不正(文字数制限)
			if (!user.getPassword().matches("^[A-Za-z0-9]$") && user.getPassword().length() < 6) {
				signupErrors.add("パスワードは6文字以上で入力してください");
			} else if(!user.getPassword().matches("^[A-Za-z0-9]$") && user.getPassword().length() > 20) {
				signupErrors.add("パスワードは20文字以下で入力してください");
			}
		}

		// 部署・支店の選択不正
		//branch取得
		Branch branch = branchRepository.getOne(user.getBranchId());
		// department取得
		Department department = departmentRepository.getOne(user.getDepartmentId());
		if (branch.getName().equals("本社")) {
			if ((department.getName().equals("営業部") || (department.getName().equals("技術部")))) {
				signupErrors.add("支社と部署の組み合わせが不正です");
			}
		} else {
			if (department.getName().equals("総務人事部") || (department.getName().equals("情報管理部"))) {
				signupErrors.add("支社と部署の組み合わせが不正です");
			}
		}

		if (signupErrors.size() > 0) {
			return false;
		}
		return true;
	}

	// ログインバリデーション
	private boolean loginValid(List<String> loginErrors, User user, User loginUser) {

		// アカウント
		if (StringUtils.isBlank(user.getAccount())) {
			loginErrors.add("アカウントが入力されていません");
		}

		// パスワード
		if (StringUtils.isBlank(user.getPassword())) {
			loginErrors.add("パスワードが入力されていません");
		}
		if (loginErrors.size() > 0) {
			return false;
		}

		// ユーザーが見つからない か アカウント停止中
		if (loginUser == null || loginUser.getIsStopped() == 1) {
			loginErrors.add("アカウントまたはパスワードが誤っています");
		}

		if (loginErrors.size() > 0) {
			return false;
		}

		return true;
	}

//		// ユーザー編集バリデーション
	private boolean editValid(List<String> editErrors, User user, User userData, String verficPass) {

		// ユーザー名を変更する場合
		if (StringUtils.isEmpty(user.getName())) {
			editErrors.add("ユーザー名を入力してください");
		} else if (!(user.getName().equals(userData.getName()))) {

			// 名前不正
			if (user.getName().length() > 10) {
				editErrors.add("ユーザー名は10文字以下にしてください");
			}
		}

		// アカウント名を変更する場合
		if (StringUtils.isEmpty(user.getAccount())) {
			editErrors.add("アカウントを入力してください");
		} else if (user.getAccount() != userData.getAccount()) {
			// アカウント重複
			List<User> users = usersService.findAllUser(); // 全件取得
			for (int i = 0; i < users.size(); i++) {
				if ((users.get(i).getId() != user.getId()) && (users.get(i).getAccount().equals(user.getAccount()))) {
					editErrors.add("アカウントが重複しています");
					break;
				}
			}

			// アカウント不正(文字数制限)
			if (!user.getAccount().matches("^[A-Za-z0-9]$") && user.getAccount().length() < 6) {
				editErrors.add("アカウントは6文字以上で入力してください");
			} else if(!user.getAccount().matches("^[A-Za-z0-9]$") && user.getAccount().length() > 20) {
				editErrors.add("アカウントは20文字以下で入力してください");
			}
		}

		if (!StringUtils.isEmpty(user.getPassword())) {
			// パスワード不一致
			if (!(user.getPassword().equals(verficPass))) {
				editErrors.add("入力したパスワードと確認用パスワードが一致しません");
			}

			// アカウント不正(文字数制限)
			if (!user.getPassword().matches("^[A-Za-z0-9]$") && user.getPassword().length() < 6) {
				editErrors.add("パスワードは6文字以上で入力してください");
			} else if(!user.getPassword().matches("^[A-Za-z0-9]$") && user.getPassword().length() > 20) {
				editErrors.add("パスワードは20文字以下で入力してください");
			}
		} else if(StringUtils.isEmpty(user.getPassword()) && verficPass != null) {
			editErrors.add("入力したパスワードと確認用パスワードが一致しません");
		}

		// 部署・支店の選択不正
		//branch取得
		Branch branch = branchRepository.getOne(user.getBranchId());
		// department取得
		Department department = departmentRepository.getOne(user.getDepartmentId());
		if (branch.getName().equals("本社")) {
			if ((department.getName().equals("営業部") || (department.getName().equals("技術部")))) {
				editErrors.add("支社と部署の組み合わせが不正です");
			}
		} else {
			if (department.getName().equals("総務人事部") || (department.getName().equals("情報管理部"))) {
				editErrors.add("支社と部署の組み合わせが不正です");
			}
		}
		if (editErrors.size() > 0) {
			return false;
		}

		return true;
	}

}
