package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.CommentsService;
import com.example.demo.service.MessagesService;
import com.example.demo.service.UsersService;

@Controller
public class ForumController {
	@Autowired
	MessagesService messageService;
	@Autowired
	CommentsService commentService;
	@Autowired
	UsersService usersService;
	@Autowired
	HttpSession session;


	//コメント投稿
	@PostMapping("/comment")
	public ModelAndView addMessage(@Validated @ModelAttribute("commentForm") Comment comment, BindingResult result) {
		String text = comment.getText();
		Integer messageId = comment.getMessageId();
		List<String> errorMessages = new ArrayList<String>();

		if (!isValidMessage(text, errorMessages)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			List<User> userData = usersService.findAllUser();
			List<Message> messageData = messageService.findAllMessage();
			List<Comment> commentData = commentService.findAllComment();
			mav.setViewName("/top");
			mav.addObject("users", userData);
			mav.addObject("messages", messageData);
			mav.addObject("comments", commentData);
			User loginUser = (User) session.getAttribute("loginUser");
			mav.addObject("loginUser", loginUser);
			mav.setViewName("/top");
			return mav;
		}

		User loginUser = (User) session.getAttribute("loginUser");
		comment.setUserId(loginUser.getId());
		comment.setMessageId(messageId);
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	//コメントバリデーション
	private boolean isValidMessage(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");
		} else if (text.length() > 500) {
			errorMessages.add("本文は500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	//メッセージ削除
	@GetMapping("/delete/message/{id}")
	public ModelAndView deleteMessage(@PathVariable Integer id) {
		messageService.deleteMessage(id);
		return new ModelAndView("redirect:/");
	}

	//コメント削除
	@GetMapping("/delete/comment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	//投稿絞り込み
	@GetMapping("/try")
	public ModelAndView top(@RequestParam(required = false, name = "startDate") String startDate,
			@RequestParam(required = false, name = "endDate") String endDate,
			@RequestParam(required = false, name = "searchCategory") String searchCategory) {

		ModelAndView mav = new ModelAndView();

		if (startDate != null) {
			mav.addObject("startDate", startDate);
		}
		if (endDate != null) {
			mav.addObject("endDate", endDate);
		}
		if (searchCategory != null) {
			mav.addObject("searchCategory", searchCategory);
		}

		List<Message> messageData;

		if (startDate != null && endDate != null && searchCategory != null) {
			messageData = messageService.findByCreatedDateAndCategory(startDate, endDate, searchCategory);
		} else {
			messageData = messageService.findAllMessage();
		}

		mav.addObject("messages", messageData);
		List<User> userData = usersService.findAllUser();
		List<Comment> commentData = commentService.findAllComment();
		mav.addObject("users", userData);
		mav.addObject("comments", commentData);

		User loginUser = (User) session.getAttribute("loginUser");
		mav.addObject("loginUser", loginUser);

		String adminError = (String) session.getAttribute("adminError");
		mav.addObject("adminError", adminError);
//		セッションのフィルターメッセージを削除
		session.removeAttribute("adminError");

		mav.setViewName("/top");

		return mav;
	}
}